#!/bin/sh
#script para configurar los permisos de drupal 8 

#ruta a la instalación de drupal
drupal_path='/var/www/html/www.ubuntu-server.test'
#Ir a la ruta de la instalación 
if [ -d $drupal_path ]
then
	cd $drupal_path;
	#cambiar permisos a todos los directorios y subirectorios dentro de la instalacion de drupal
	find . -type d -exec chown -R root:www-data '{}' \;
	find . -type d -exec chmod u=rwx,g=rx,o= '{}' \;

	find . -type d -name modules -exec chown -R www-data:www-data '{}' \;
	find . -type d -name modules -exec chmod ug=rwx,o= '{}' \;

	find . -type d -name themes -exec chown -R www-data:www-data '{}' \;
	find . -type d -name themes -exec chmod ug=rwx,o= '{}' \;

	find . -type d -name sites -exec chown -R www-data:www-data '{}' \;
	find . -type d -name sites -exec chmod ug=rwx,o= '{}' \;

	find . -type f -exec chown -R root:www-data '{}' \;
	find . -type f -exec chmod u=rw,g=r,o= '{}' \;
	chmod 770 /var/www/html/www.ubuntu-server.test/sites/default/settings.php                                                 
 else
	echo "No existe el path a instalación de drupal"
fi
